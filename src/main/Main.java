package main;

import java.util.ArrayList;

public class Main {
	public static void main(String[] args) {
		ArrayList<String> names = new ArrayList<>(); // <> -> Milyen t�pust t�rol, majd a neve
	    names.add("This is a name"); //
	    String kane = "Harry Kane";
	    names.add(kane);

	    ArrayList<Integer> numbers = new ArrayList<>();
	    numbers .add(23);
	    for(int i = 0; i < 25; i++){
	      numbers.add(i);
	    }
	    for(int i = 0; i< numbers.size(); i++){
	      System.out.println(numbers.get(i)); //kiiratod, hogy az elemeket
	    }
	    numbers.remove(8);
	    for(int tmpNumber : numbers){
	      System.out.println(tmpNumber);
	    }
	}
}
